## walleye-user 8.1.0 OPM1.171019.011 4448085 release-keys
- Manufacturer: xiaomi
- Platform: atoll
- Codename: miatoll
- Brand: Xiaomi
- Flavor: lineage_miatoll-userdebug
- Release Version: 12
- Id: SP2A.220505.002
- Incremental: 1652257722
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/walleye/walleye:8.1.0/OPM1.171019.011/4448085:user/release-keys
- OTA version: 
- Branch: walleye-user-8.1.0-OPM1.171019.011-4448085-release-keys-random-text-1581216993850
- Repo: xiaomi_miatoll_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
